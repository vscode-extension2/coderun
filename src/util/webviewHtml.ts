interface IWebviewContent {
	stdout?: string | ''
	stderr?: string | ''
	code?: number
	fileName?: string
	timeElapsed?: number
}

export default function webviewHtml({ stdout, stderr, code, fileName, timeElapsed }: IWebviewContent) {

	const out = stdout && stdout.length > 1 ? `<pre>${stdout}</pre>` : '';
	const err = stderr && stderr.length > 1 ? `<pre class="red">${stderr}</pre>` : '';
	const done = timeElapsed ? `<pre class="light">[Done] Program exit with code (${code || 0}) in ${timeElapsed} seconds</pre>` : '';

	const localeTime = new Date().toLocaleTimeString();	
	const running = `<pre class="green">[Running] ${fileName || ''} (${localeTime})</pre>`;

	return `<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Output | CodeRun</title>
	<style>
		body { font-family: Consolas; font-size: 0.9rem; }
		pre { color:#fff; line-height: 2; word-break: break-word; white-space: pre-wrap; }
		.light { color: #999999 }
		.red { color: #ff4141 }
		.green { color: #97ff97 }
	</style>
</head>
<body>	
	${running}
	${out}
	${err}
	${done}	
</body>
</html>`;
}