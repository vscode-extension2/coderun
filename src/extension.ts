import * as vscode from 'vscode';
import createPanel from './util/createPanel';
import Runner from './util/Runner';

let panel: vscode.WebviewPanel;

export function activate(context: vscode.ExtensionContext) {

	let disposable = vscode.commands.registerCommand('CodeRun.run', () => {
		if (!panel) panel = createPanel();
		Runner.executeIn(panel);
		panel.onDidDispose(e => { panel = null; });
	});

	vscode.window.showInformationMessage('CodeRun extension is activated..')

	context.subscriptions.push(vscode.commands.registerCommand('CodeRun.stop', () => {
		Runner.kill(1)
	}));

	context.subscriptions.push(disposable);
}

export function deactivate() {
	if (panel) {
		panel.dispose();
		panel = null;
	}
}