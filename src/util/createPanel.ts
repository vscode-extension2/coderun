import * as vscode from 'vscode';

export default function createPanel(): vscode.WebviewPanel {
	return vscode.window.createWebviewPanel('CodeRun', 'Output | CodeRun', vscode.ViewColumn.Beside, {});
}