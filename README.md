# CodeRun

Simple vscode extension for running files or selection: 
- Javascript
- Typescript
- Python 3
- Java

## Notes
- Typescript: Required packages ts-node and Typescript installed globally on your computer.

## Preview
![Noderel](https://gitlab.com/vscode-extension2/coderun/-/raw/master/preview.gif)

**Enjoy!**

# License
MIT