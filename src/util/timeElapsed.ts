const timeElapsed = (startTime: number) => {
  return (Date.now() - startTime) / 1000
}

export default timeElapsed