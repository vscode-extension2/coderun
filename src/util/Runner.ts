import * as vscode from 'vscode';
import { spawn } from 'cross-spawn';
import { ChildProcessWithoutNullStreams, SpawnOptionsWithoutStdio } from 'child_process';
import webviewHtml from './webviewHtml';
import timeElapsed from './timeElapsed';
import path = require('path');

const spOptions: SpawnOptionsWithoutStdio = {
  timeout: 25000,
  env: { FORCE_COLOR: '1', NPM_CONFIG_COLOR: 'always', ...process.env },
  stdio: 'pipe',
}

export default class Runner {

  private static spawnProcess: ChildProcessWithoutNullStreams;

  private static getConfig(languageId: string): Array<string> {
    return vscode.workspace.getConfiguration('CodeRun')[languageId];
  }

  private static getFilenameFromPath(fullPath: string): string {
    return path.basename(fullPath);
  }

  private static getFilePath(): string {
    return vscode.window.activeTextEditor.document.fileName;
  }

  private static getCommand(): Array<string> {
    const activeTextEditor = vscode.window.activeTextEditor;
    const languageId = activeTextEditor?.document.languageId;
    const selection = activeTextEditor.document.getText(activeTextEditor.selection).trim();

    let command = this.getConfig(languageId);

    if (!command) return;

    if (selection) command.push(selection);
    else {
      command = command.slice(0, 1);
      command.push(this.getFilePath())
    }

    return command
  }

  static executeIn(panel: vscode.WebviewPanel) {
    const command = this.getCommand();
    const fileName = this.getFilenameFromPath(this.getFilePath());

    let stdout = '';
    let stderr = '';
    let startTime = Date.now();

    this.spawnProcess = spawn(command[0], command.slice(1), spOptions);

    panel.webview.html = webviewHtml({ fileName });

    this.spawnProcess.stdout.on('data', (data) => {
      stdout += data.toString();
      panel.webview.html = webviewHtml({ stdout, stderr, fileName, timeElapsed: timeElapsed(startTime) });
    });

    this.spawnProcess.stderr.on('data', (data) => {
      stderr += data.toString();
      panel.webview.html = webviewHtml({ stderr, stdout, fileName, timeElapsed: timeElapsed(startTime) });
    });

    this.spawnProcess.on('close', (code: number) => {
      panel.webview.html = webviewHtml({ code, stdout, stderr, fileName, timeElapsed: timeElapsed(startTime) });
    });
  }

  static kill(signal?: number) {
    this.spawnProcess.kill(signal);
  }
}